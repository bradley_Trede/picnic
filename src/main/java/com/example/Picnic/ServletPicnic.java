package com.example.Picnic;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.PrintWriter;


@WebServlet
        (name = "ServletPicnic", urlPatterns = {"/ServletPicnic"})
public class ServletPicnic extends HttpServlet {

    protected void doPost(HttpServletRequest request, HttpServletResponse response) {

        // Read from form fields
        try {
            PrintWriter out = response.getWriter();
            response.setContentType("text/html");
            out.println("<html><head></head><body>");

            String employee = request.getParameter("employee");
            String peopleCount = request.getParameter("peopleCount");
            String dish = request.getParameter("dish");
            String fireworks = request.getParameter("fireworks");


            // Print results with thank you
            // build HTML code
            try {


                out.println("<h1>Picnic Submission Confirmation</h1>");
                out.println("<p>Employee Name" + employee + "</p>");
                out.println("<p>The Number of People Attending is " + peopleCount + "</p>");
                out.println("<p>Wow, " + dish + " sounds delicious</p>");
                if (fireworks != "No") {
                    out.println("<p>Bring a blanket to watch the fireworks </p>");
                } else {
                    out.println("<p>Sorry to hear you are missing the fireworks </p>");
                }

                out.println("</body></html>");


            } catch (Exception e) {
                System.out.println("Unable to terminate correctly");
            }

            // Send over JSON
            class addtoJSON {


                public String SignupToJSON(Signup signup) {


                    ObjectMapper mapper = new ObjectMapper();
                    String str = "";


                    try {
                        str = mapper.writeValueAsString(signup);
                    } catch (JsonProcessingException e) {
                        System.err.println(e.toString());
                    }

                    return str;



                }
            }
        } catch (Exception e) {
        }
    }
}